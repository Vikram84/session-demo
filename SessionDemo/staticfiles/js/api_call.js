function fetch_all_count(){
    var all = $('#all_count');

    $.ajax(
        {
            url: 'all_count',
            method: 'GET',
            success: function(response){
                all.val(response['all']);
            }
        }
    );
}

function fetch_your_count(){
    var ur = $('#your_count');

    $.ajax(
        {
            url: 'your_count',
            method: 'GET',
            success: function(response){
                ur.val(response['your']);
            }
        }
    );
}