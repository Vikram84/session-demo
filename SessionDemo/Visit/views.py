from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout

all_count = 0

# Create your views here.
def visit(req):
    global all_count
    all_count += 1      
    if req.user.is_active:
        count = req.session.get('my_visits', 0)
        if count:
            req.session['my_visits'] += 1
        else:
            req.session['my_visits'] = 1
        return render(req, "index.html", {'all_count': all_count, 'your_count': req.session['my_visits']})
    else:
        return render(req, "index.html", {'all_count': all_count})

def fetch_all_count(req):
    return JsonResponse({'all': all_count})

def fetch_admin_count(req):
    return JsonResponse({'your': req.session.get('my_visits',0)})

def admin_login(req):
    usr = authenticate(username="admin", password="secret")
    login(req, usr)
    return redirect("home")

def admin_logout(req):
    logout(req)
    return redirect("home")
